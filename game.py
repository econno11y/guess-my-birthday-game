from random import randint

name = input("Hi, what is your name? ")
number = 1
while number < 5:
    random_between_1_and_12 = randint(1, 12)
    month = (
        f"0{random_between_1_and_12}"
        if random_between_1_and_12 < 10
        else random_between_1_and_12
    )
    year = randint(1920, 2015)
    print(f"Guess {number}: {name} were you born in {month}/{year}?\n")
    answer = input("Yes or no? ")

    if answer.lower() == "yes":
        print("Yes, I knew it!")
        exit
    else:
        print("Drat! Let me try again!")
        number += 1
print("I have better things to do!")
exit
